import logparser
import re
import json
from datetime import datetime

def parse_log_file(line):
    log_format = '%time %level %msg'
    try:
        parsed_line = logparser.parse(line, headlines=1, taillines=1)
        return parsed_line
        return {'time': parsed_line['time'], 'level': parsed_line['level'], 'msg': parsed_line['msg']}
    except:
        return {}



def parse_syslog(line):
    regex = r'^(\S+\s+\d+\s+\S+)\s+(\S+)\s+(\S+)\[(\d+)\]\s+\((\S+)\)\:\s+(.*)'
    match = re.match(regex, line)
    if match is None:
        regex = r'(\w{3}\s\d{1,2}\s\d{2}:\d{2}:\d{2})\s(\S+)\s(\S+)\[(\d+)\]:\s(.*)'
        match = re.match(regex, line)
    if match:
        log_dict = {
            'timestamp': match.group(1) if match.group(1) else '',
            'hostname': match.group(2) if match.group(2) else '',
            'process': match.group(3) if match.group(3) else '',
            'pid': match.group(4) if match.group(4) else '',
            'message': match.group(5) if match.group(5) else '',
            #'message2': match.group(6) if match.group(6) else ''
        }
        return log_dict
    else:
        return None

# Exemple d'utilisation
#syslog_string =
""" 
Apr 25 10:30:45 mypc sshd[2582]: Accepted password for myuser from 192.168.1.100 port 54321 ssh2
Apr 25 11:20:10 mypc kernel: [12345.678] CPU0: Core temperature above threshold, cpu clock throttled
"""
#logs = parse_syslog(syslog_string)

# Enregistrer les logs sous forme de fichier JSON
"""with open('logs.json', 'w') as f:
    json.dump(logs, f, indent=4)

# Afficher les logs en console
print(json.dumps(logs, indent=4))"""
