from typing import Union
from syslogmp import parse
from fastapi import FastAPI, File, UploadFile, HTTPException
from fastapi.responses import FileResponse
from function import parse_log_file, parse_syslog
import logparser
import os, datetime
import json

app = FastAPI()

LOG_DIR = "logs"

@app.get("/")
def read_root():
    return {"Hello": "World"}

@app.post("/uploadfile/")
async def create_upload_file(file: UploadFile = File(...)):
    with open(os.path.join("logs", file.filename), "wb") as buffer:
        buffer.write(await file.read())
    return {"filename": file.filename}

@app.get("/list_logs/")
async def read_logs():
    files = os.listdir(LOG_DIR)
    return {"files": files}


@app.get("/logs/{file_name}")
async def get_logs(file_name: str, start: int = 0, limit: int = 10):
    log_path = os.path.join(LOG_DIR, file_name)
    if not os.path.exists(log_path):
        raise HTTPException(status_code=404, detail="File not found")

    with open(log_path, "r") as f:
        log_lines = f.readlines()

    num_lines = len(log_lines)

    if start >= num_lines:
        raise HTTPException(status_code=400, detail="Invalid start position")

    end = min(start + limit, num_lines)
    log_lines = log_lines[start:end]

    parsed_logs = []
    for line in log_lines:
        parsed_logs.append(parse_log_file(line))

    return parsed_logs

@app.post('/parse_logs/{filename}')
async def parse_logs(filename: str):
    try:
        log_path = os.path.join(LOG_DIR, filename)
        if not os.path.exists(log_path):
            raise HTTPException(status_code=404, detail="File not found")
        with open(log_path, 'r') as f:
            parsed_logs = []
            #logs = f.readlines()
            for line in f:
                parsed_log = parse_syslog(line)
                parsed_logs.append(parsed_log)
            json_logs = json.dumps(parsed_logs, indent=4)
            
        #print(parsed_log)
        #parsed_logs.append(parsed_log)
        #parsed_data = logparser.parse(log_string)
        #print(parsed_data)
        
        #json_logs = parse_syslog(logs)
        json_path = os.path.join('json', filename + '.json')
        with open(json_path, 'w') as f:
            f.write(json_logs)
        return FileResponse(json_path, filename=filename+'.json', headers={'Content-Disposition': 'attachment'})
    except Exception as err:
        raise HTTPException(status_code=400, detail=str(err))