import os
import logparser
from fastapi import FastAPI, HTTPException

app = FastAPI()

LOG_DIR = '/path/to/your/logs/directory'

def parse_log_file(line):
    log_format = '%time %level %msg'
    parsed_line = logparser.parse(line, log_format)
    return {'time': parsed_line['time'], 'level': parsed_line['level'], 'msg': parsed_line['msg']}

@app.get("/logs/{file_name}")
async def get_logs(file_name: str):
    log_path = os.path.join(LOG_DIR, file_name)
    if not os.path.exists(log_path):
        raise HTTPException(status_code=404, detail="File not found")

    with open(log_path, "r") as f:
        log_lines = f.readlines()

    parsed_logs = []
    for line in log_lines:
        parsed_logs.append(parse_log_file(line))

    return parsed_logs

if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8002) # Change the port number here
